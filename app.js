import express from 'express'
import { config } from 'dotenv'

import { newsController } from './src/controllers/news-controller.js'
import { userController } from './src/controllers/user-controller.js'

config()
const app = express()

app.use(express.json())

app.use('/user', userController)
app.use('/news', newsController)

const port = process.env.PORT || 3000
app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`)
})