import express from 'express'
import jwt from 'jsonwebtoken'

import { User } from '../models/User.model.js'
import { UserService } from '../services/UserService.js'
import { MockUserRepository } from '../repository/MockUserRepository.js'
import { isAuthenticated } from '../middleware/authenticated.js'


// TODO: Refresh token & expiration on access token
export const userController = express.Router()

const userService = new UserService(new MockUserRepository())

userController.post('/register', async (req, res, next) => {
    const { username, password, confirmPassword } = req.body

    if (!username || !password || !confirmPassword || password !== confirmPassword || !userService.usernameUniqueConstraint(username)) {
        return res.sendStatus(400)
    }

    const user = await User.createUser(username, password)
    
    if (user) {
        userService.createUser(user)
        return res.sendStatus(201)
    } 

    return res.sendStatus(500)
})

userController.post('/login', async (req, res, next) => {
    const { username, password } = req.body

    if (!username || !password) {
        return res.sendStatus(400)
    }

    const user = userService.getUsers(username)[0]

    if (!user) {
        return res.sendStatus(400)
    }

    try {
        if (await user.comparePasswords(password)) {
            const claims = { id: user.id }
            const accessToken = jwt.sign(claims, process.env.ACCESS_TOKEN_SECRET)

            return res.json({ accessToken })
        } else {
            return res.sendStatus(401)
        }
    } catch {
        return res.sendStatus(500)
    }
})

userController.get('/list', isAuthenticated, (req, res, next) => {
    const { username } = req.query

    const users = userService.getUserList(username)
    return res.json(users)
})

userController.get('/:id', isAuthenticated, (req, res, next) => {
    const { id } = req.params

    const user = userService.getUser(id)
    return res.json(user)
})

