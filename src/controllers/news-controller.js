import express from 'express'

import { News } from '../models/News.model.js'
import { MockNewsRepository } from '../repository/MockNewsRepository.js'
import { NewsService } from '../services/NewsService.js'
import { isAuthenticated } from '../middleware/authenticated.js'
import { UserDto } from '../models/User.model.js'

export const newsController = express.Router()


const newsService = new NewsService(new MockNewsRepository())

// TODO: if input model data fails validation return 400 (Bad request)
newsController.post('/', isAuthenticated, (req, res, next) => {
    const { title, body } = req.body
    
    const news = new News(title, body, new UserDto(req.user.id, req.user.username))
    newsService.createNews(news)
    return res.sendStatus(201)
})

// TO DO: Add page & pageSize params for pagination as query params
newsController.get('/list', (req, res, next) => {
    const { title } = req.query

    const newsList = newsService.getNewsList(title)

    return res.json(newsList)
})

newsController.get('/:id', (req, res, next) => {
    const { id } = req.params

    const news = newsService.getNews(id)

    if (!news) {
        return res.sendStatus(404)
    }

    return res.json(news)
})

// TODO: if input model data fails validation return 400 (Bad request)
newsController.put('/:id/update', isAuthenticated, (req, res, next) => {
    const { id } = req.params
    const { title, body } = req.body

    const oldNews = newsService.getNews(id)
    if (oldNews.author.id !== req.id) {
        return res.statusCode(401)
    }

    if (!oldNews) {
        return res.sendStatus(404)
    }
    
    const newNews = new News(title, body)
    newsService.updateNews(oldNews, newNews)

    return res.sendStatus(204)
})

newsController.delete('/:id', isAuthenticated, (req, res, next) => {
    const { id } = req.params

    const news = newsService.getNews(id)

    if (!news) {
        return res.sendStatus(404)
    }

    newsService.deleteNews(id)

    return res.sendStatus(200)
})
