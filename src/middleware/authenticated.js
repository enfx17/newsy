import jwt from 'jsonwebtoken'

import { MockUserRepository } from '../repository/MockUserRepository.js'
import { UserService } from '../services/UserService.js'

const userService = new UserService(new MockUserRepository())

// TODO: check for token expiration time
export async function isAuthenticated(req, res, next) {
    const authHeader = req.headers.authorization
    const accessToken = authHeader && authHeader.split('Bearer ')[1]

    if (!accessToken) {
        return res.sendStatus(401)
    }

    jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (err, claims) => {
        if (err) return res.sendStatus(403)

        const user = userService.getUser(claims.id)
        if (!user) {
            return res.sendStatus(403)
        } else {
            req.user = user
            next()
        }
    })
}