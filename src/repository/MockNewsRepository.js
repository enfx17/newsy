import { News } from '../models/News.model.js'

// This is where you write your database queries
// For this example we are mocking the database with a single array
export class MockNewsRepository {

    static news = []

    createNews(news) {
        news.generateId()
        MockNewsRepository.news.push(news)
    }

    getNews(id) {
        return MockNewsRepository.news.find(news => news.id === id)
    }

    // TO DO: Add pagination & search by body
    getNewsList(title) {
        return !title ? MockNewsRepository.news : MockNewsRepository.news.filter((news) => news.title.toLowerCase().includes(title.toLowerCase()))
    }

    deleteNews(id) {
        const news = this.getNews(id)
        const newsIndex = MockNewsRepository.news.indexOf(news)
        MockNewsRepository.news.splice(newsIndex, 1)
    }

    updateNews(oldNews, newNews) {
        MockNewsRepository.news = MockNewsRepository.news.map(
            (news) => news.id === oldNews.id ? 
            new News(newNews.title, newNews.body, oldNews.id) : news
        )
    }
    
}