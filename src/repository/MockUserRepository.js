import { User } from '../models/User.model.js'

export class MockUserRepository {

    static users = []

    addUser(user) {
        MockUserRepository.users.push(user)
    }

    getUser(id) {
        return MockUserRepository.users.find(user => user.id === id)
    }

    getUserList(username) {
        return !username ? MockUserRepository.users : MockUserRepository.users.filter(user => username === user.username)
    }

    
}