import bcrypt from 'bcrypt'

import { BaseModel } from './BaseModel.model.js'

// TODO: Model validation, usually implemented by ORM of choice
export class User extends BaseModel {

    username
    hashedPassword

    constructor(username, hashedPassword) {
        super()
        this.username = username
        this.hashedPassword = hashedPassword
    }

    static async createUser(username, password) {
        try {
            const salt = await bcrypt.genSalt()
            const hashedPassword = await bcrypt.hash(password, salt)
            const user = new User(username, hashedPassword)
            user.generateId()
            return user
        } catch {
            return null
        }
    }

    async comparePasswords(password) {
        return await bcrypt.compare(password, this.hashedPassword) 
    }
}

export class UserDto {

    id
    username

    constructor(id, username) {
        this.id = id
        this.username = username
    }

}