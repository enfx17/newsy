import shortid from 'shortid'

export class BaseModel {
    id

    generateId() {
        this.id = shortid.generate()
    }
}
