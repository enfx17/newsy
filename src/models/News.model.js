import { BaseModel } from './BaseModel.model.js'

// TODO: Model validation, usually implemented by ORM of choice
export class News extends BaseModel {

    title
    body

    constructor(title, body, author, id) {
        super()
        this.title = title
        this.body = body
        this.author = author
        this.id = id
    }

    getSummary() {
        return this.body.substring(0, 100).trim() + '...'
    }
}

export class NewsPreviewDto {

    id
    title
    summary
    
    constructor(id, title, summary) {
        this.id = id
        this.title = title
        this.summary = summary
    }
}