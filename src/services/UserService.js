import { UserDto } from '../models/User.model.js'

export class UserService {

    userRepo

    constructor(repository) {
        this.userRepo = repository
    }

    createUser(user) {
        this.userRepo.addUser(user)
    }

    getUser(id) {
        const user = this.userRepo.getUser(id)
        return user ? new UserDto(user.id, user.username) : null
    }

    getUsers(username) {
        return this.userRepo.getUserList(username) 
    }

    getUserList(username) {
        return this.getUsers(username).map(user => new UserDto(user.id, user.username))
    }
    
    // This should be done by model validation
    usernameUniqueConstraint(username) {
        return !this.userRepo.getUserList(username).length
    }
}