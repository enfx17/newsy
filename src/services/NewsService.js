import { NewsPreviewDto } from '../models/News.model.js'

// This is where your write your bussiness logic
export class NewsService {

    newsRepo

    constructor(newsRepo) {
        this.newsRepo = newsRepo
    }

    createNews(news) {
        this.newsRepo.createNews(news)
    }

    getNewsList(title) {
        return this.newsRepo.getNewsList(title).map((news) => new NewsPreviewDto(news.id, news.title, news.getSummary()))
    }

    getNews(id) {
        return this.newsRepo.getNews(id)
    }

    updateNews(oldNews, newNews) {
        this.newsRepo.updateNews(oldNews, newNews)
    }

    deleteNews(id) {
        this.newsRepo.deleteNews(id)
    }
}